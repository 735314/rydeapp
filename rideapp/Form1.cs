﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rideapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Welcome to Ryde App");
            string from = textBox1.Text;
            string to = textBox2.Text;
            double basefare = 2.50;
            double distancecharge = 0.81;
            double servicefare = 1.75;
            double totalprice = 0.0;
            string rb;
            double minimumfare= 5.50;
            TimeSpan time=DateTime.Now.TimeOfDay;

            if (from == "")
            { MessageBox.Show("Enter current location"); }
            else if (to == "")
            { MessageBox.Show("Enter destination location"); }
            else if (from == "Fairview Mall" && to == "Tim Hortons")
            {
                if (radioButton1.Checked == true)
                {
                    rb = radioButton1.Text;
                    if((time>new TimeSpan(10,00,00)&& time<new TimeSpan(12,00,00))||(time>new TimeSpan(16,00,00)&& time<new TimeSpan(18,00,00))||(time>new TimeSpan(20,00,00)&& time<new TimeSpan(21,00,00)))
                    {
                        distancecharge = 0.972 * 1.2;
                        totalprice = basefare + distancecharge + servicefare;
                        if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                        else
                         { MessageBox.Show("Price less than minimum fare charge"); }
                    }
                    else
                    { distancecharge = 0.81 * 1.2;
                    totalprice = basefare + distancecharge + servicefare;
                       if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                       else
                    { MessageBox.Show("Price less than minimum fare charge"); }}
                   
                }
                else if (radioButton2.Checked == true)
                {
                    rb = radioButton2.Text;
                    if((time>new TimeSpan(10,00,00)&& time<new TimeSpan(12,00,00))||(time>new TimeSpan(16,00,00)&& time<new TimeSpan(18,00,00))||(time>new TimeSpan(20,00,00)&& time<new TimeSpan(21,00,00)))
                    {
                        distancecharge = 1.116 * 1.2;
                        totalprice = basefare + distancecharge + servicefare;
                        if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                        else
                         { MessageBox.Show("Price less than minimum fare charge"); }
                    }
                    else
                    {distancecharge = 1.2 * 0.93;
                    servicefare = 2.75;
                    totalprice = basefare + distancecharge + servicefare;
                    if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                    else
                    { MessageBox.Show("Price less than minimum fare charge"); }
                    }
                }
                else
                { MessageBox.Show("Select type of ryde"); }
            }



            else if (from == "275 Yorkland Blvd" && to == "CN Tower")
            {
                if (radioButton1.Checked == true)
                {
                    rb = radioButton1.Text;
                    if((time>new TimeSpan(10,00,00)&& time<new TimeSpan(12,00,00))||(time>new TimeSpan(16,00,00)&& time<new TimeSpan(18,00,00))||(time>new TimeSpan(20,00,00)&& time<new TimeSpan(21,00,00)))
                    {
                        distancecharge = 0.972 * 22.9;
                        totalprice = basefare + distancecharge + servicefare;
                        if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                        else
                         { MessageBox.Show("Price less than minimum fare charge"); }
                    }
                    else
                        {
                    distancecharge = 0.81 * 22.9;
                    totalprice = basefare + distancecharge + servicefare;
                    if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                    else
                    { MessageBox.Show("Price less than minimum fare charge"); }}
                    
                }
                else if (radioButton2.Checked == true)
            {
                rb = radioButton2.Text;
                    if((time>new TimeSpan(10,00,00)&& time<new TimeSpan(12,00,00))||(time>new TimeSpan(16,00,00)&& time<new TimeSpan(18,00,00))||(time>new TimeSpan(20,00,00)&& time<new TimeSpan(21,00,00)))
                    {
                        distancecharge = 1.116 * 22.9;
                        totalprice = basefare + distancecharge + servicefare;
                        if (minimumfare < totalprice)
                    {
                        MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                    }
                        else
                         { MessageBox.Show("Price less than minimum fare charge"); }
                    }
                    else 
                    {
                        distancecharge = 22.9 * 0.93;
                servicefare = 2.75;
                totalprice = basefare + distancecharge + servicefare;
                    if (minimumfare < totalprice)
                {
                    MessageBox.Show(rb + " Confirmed!" + "\n" + " From: " + from + "\n" + "To: " + to + "\n" + "Booking Fee: " + basefare + "\n" + "Distance Charge: " + distancecharge + "\n" + "Service Fee: " + servicefare + "\n" + "Total: " + totalprice);
                }
                     else
                { MessageBox.Show("Price less than minimum fare charge"); }
                     }
            }
                
                else
                { MessageBox.Show("Select type of ryde"); }
            }
        else
            {
                MessageBox.Show("Invalid location");
            }
        }        
}
}